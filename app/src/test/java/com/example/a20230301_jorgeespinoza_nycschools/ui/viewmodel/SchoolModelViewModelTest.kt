package com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetSchoolsUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.SearchSchoolsUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolModelViewModelTest{
    @RelaxedMockK
    private lateinit var getSchoolsUseCase: GetSchoolsUseCase
    @RelaxedMockK
    private lateinit var searchSchoolsUseCase: SearchSchoolsUseCase

    private lateinit var schoolViewModel: SchoolViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        schoolViewModel = SchoolViewModel(getSchoolsUseCase, searchSchoolsUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when viewmodel is created at the first time, get all schools and set them`() = runTest{
        val schoolModels: List<SchoolDomain> = listOf(SchoolDomain("","","","","","",""))
        coEvery { getSchoolsUseCase() } returns schoolModels

        schoolViewModel.getSchools()

        assert(schoolViewModel.modelSchools.value == schoolModels)
    }

    @Test
    fun `if searchSchoolsUseCase returns an empty list then return null`() = runTest{
        coEvery { searchSchoolsUseCase("") } returns emptyList()

        schoolViewModel.searchSchools("")

        assert(schoolViewModel.modelSchools.value == null)
    }
}