package com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230301_jorgeespinoza_nycschools.data.util.NetworkConnectionHelper
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetLastLocalSchoolScoreUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetSchoolScoresUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.SaveLocalSchoolScoreUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolModelScoreViewModelTest{
    @RelaxedMockK
    private lateinit var getSchoolScoresUseCase: GetSchoolScoresUseCase
    private lateinit var getLastLocalSchoolScoreUseCase: GetLastLocalSchoolScoreUseCase
    private lateinit var saveLocalSchoolScoreUseCase: SaveLocalSchoolScoreUseCase
    private lateinit var network: NetworkConnectionHelper
    private lateinit var schoolScoreViewModel: SchoolScoreViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        schoolScoreViewModel = SchoolScoreViewModel(getSchoolScoresUseCase, getLastLocalSchoolScoreUseCase, saveLocalSchoolScoreUseCase, network)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when viewmodel get null scores then set default data`() = runTest{
        coEvery { getSchoolScoresUseCase("") } returns emptyList()

        schoolScoreViewModel.getSchoolScore("")

        assert(schoolScoreViewModel.modelSchoolScore.value == null)
    }
}