package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.repository.SchoolRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test


class GetSchoolsUseCaseTest{
    @RelaxedMockK
    private lateinit var schoolRepository: SchoolRepository

    lateinit var getSchoolsUseCase: GetSchoolsUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        getSchoolsUseCase = GetSchoolsUseCase(schoolRepository)
    }

    @Test
    fun `when the api doesnt return anything then get values stored local`() = runBlocking {

        coEvery { schoolRepository.getAllSchools() } returns emptyList()

        getSchoolsUseCase()

        verify(exactly = 1) { schoolRepository.getLocalSchools() }
    }
}