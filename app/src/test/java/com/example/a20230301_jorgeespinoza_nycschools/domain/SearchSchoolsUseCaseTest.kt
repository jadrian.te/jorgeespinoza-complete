package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.provider.SchoolProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Before
import org.junit.Test

class SearchSchoolsUseCaseTest{
    @RelaxedMockK
    private lateinit var schoolProvider: SchoolProvider

    lateinit var searchSchoolsUseCase: SearchSchoolsUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        searchSchoolsUseCase = SearchSchoolsUseCase(schoolProvider)
    }

    @Test
    fun `when the characters searched not match with any school then return null`() {
        every{ schoolProvider.schoolModels } returns emptyList()

        val response = searchSchoolsUseCase("qwerty")

        assert(response == null)
    }
}