package com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetSchoolsUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.SearchSchoolsUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val getSchoolsUseCase: GetSchoolsUseCase,
    private val searchSchoolsUseCase: SearchSchoolsUseCase
): ViewModel() {

    val modelSchools = MutableLiveData<List<SchoolDomain>>()
    val isLoading = MutableLiveData<Boolean>()
    val skipSplash = MutableLiveData<Boolean>()
    val swipeRefresh = MutableLiveData<Boolean>()
    val tryAgain = MutableLiveData<Boolean>()

    fun getSchools(){
        viewModelScope.launch{
            isLoading.postValue(true)
            val schools = getSchoolsUseCase()
            if (schools.isNotEmpty()) {
                tryAgain.postValue(false)
                modelSchools.postValue(schools)
            }else
                tryAgain.postValue(true)
            isLoading.postValue(false)
            skipSplash.postValue(true)
        }
    }

    fun getSchoolsRefresh(){
        viewModelScope.launch{
            val schools = getSchoolsUseCase()
            if (schools.isNotEmpty())
                modelSchools.postValue(schools)
            swipeRefresh.postValue(false)
        }
    }

    fun searchSchools( school: String ){
        val schools = searchSchoolsUseCase(school) ?: emptyList()
        if(schools.isNotEmpty())
            modelSchools.postValue(schools)

    }

}