package com.example.a20230301_jorgeespinoza_nycschools.ui.view.adapter


import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230301_jorgeespinoza_nycschools.databinding.SchoolRowBinding
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import io.reactivex.subjects.PublishSubject

class SchoolViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val binding = SchoolRowBinding.bind(view)

    fun render(school: SchoolDomain, click: PublishSubject<SchoolDomain> ) {
        binding.tvName.text = school.name
        binding.tvlocation.text = school.location
        binding.cvSchool.setOnClickListener { click.onNext(school) }
    }
}