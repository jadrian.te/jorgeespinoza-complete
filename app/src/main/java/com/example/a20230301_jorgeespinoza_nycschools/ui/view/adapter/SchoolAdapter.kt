package com.example.a20230301_jorgeespinoza_nycschools.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230301_jorgeespinoza_nycschools.R
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import io.reactivex.subjects.PublishSubject


class SchoolAdapter(private val data: List<SchoolDomain>): RecyclerView.Adapter<SchoolViewHolder>() {

    private val click = PublishSubject.create<SchoolDomain>()
    val clickEvent: io.reactivex.Observable<SchoolDomain> = click

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.school_row, viewGroup, false)
        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: SchoolViewHolder, position: Int) {
        val school = data[position]
        viewHolder.render(school, click)
    }

    override fun getItemCount() = data.size
}

