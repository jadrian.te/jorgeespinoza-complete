package com.example.a20230301_jorgeespinoza_nycschools.ui.view


import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewTreeObserver
import androidx.activity.viewModels
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230301_jorgeespinoza_nycschools.databinding.ActivityMainBinding
import com.example.a20230301_jorgeespinoza_nycschools.ui.view.adapter.SchoolAdapter
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.Disposable

@AndroidEntryPoint
class MainActivity: AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val schoolViewModel: SchoolViewModel by viewModels()
    private lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate( layoutInflater )
        setContentView(binding.root)
        //splashScreen.setKeepOnScreenCondition{ false }

        initActivity(savedInstanceState)
    }

    private fun initActivity( savedInstanceState: Bundle? ){
        when(resources.configuration.orientation){
            Configuration.ORIENTATION_PORTRAIT -> binding.rcSchools.layoutManager = LinearLayoutManager( this )
            Configuration.ORIENTATION_LANDSCAPE -> binding.rcSchools.layoutManager = GridLayoutManager( this, 2 )
            else -> binding.rcSchools.layoutManager = LinearLayoutManager( this )
        }

        schoolViewModel.modelSchools.observe( this, Observer { it ->
            binding.etSearchSchools.isVisible = it.isNotEmpty()
            binding.tilSearchSchools.isVisible = it.isNotEmpty()
            val schoolAdapter = SchoolAdapter(it)
            binding.rcSchools.adapter = schoolAdapter
            disposable = schoolAdapter.clickEvent.subscribe {
                onItemSelected(it)
            }
        } )

        schoolViewModel.isLoading.observe( this,  Observer {
            binding.prgBar.isVisible = it
            binding.efabRefresh.isEnabled = !it
        } )

        schoolViewModel.tryAgain.observe( this, Observer {
            binding.efabRefresh.isVisible = it
        } )

        binding.etSearchSchools.addTextChangedListener {
            schoolViewModel.searchSchools( it.toString() )
        }

        binding.root.viewTreeObserver.addOnPreDrawListener(
            object: ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    return if (schoolViewModel.skipSplash.value == true) {
                        binding.root.viewTreeObserver.removeOnPreDrawListener(this)
                        true
                    } else
                        false
                }
            }
        )
        binding.srSchools.setOnRefreshListener {
            schoolViewModel.getSchoolsRefresh()
        }
        schoolViewModel.swipeRefresh.observe( this, Observer {
            binding.srSchools.isRefreshing = it
        } )
        binding.efabRefresh.setOnClickListener {
            schoolViewModel.getSchools()
        }
        if(savedInstanceState == null)
            schoolViewModel.getSchools()
    }

    private fun onItemSelected(school: SchoolDomain)
    {
        val intent = Intent(this, SchoolDetailsActivity::class.java)
        intent.putExtra("school", school)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}