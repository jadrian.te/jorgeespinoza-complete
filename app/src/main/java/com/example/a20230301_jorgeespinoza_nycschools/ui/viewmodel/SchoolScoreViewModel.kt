package com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScoreLocal
import com.example.a20230301_jorgeespinoza_nycschools.data.util.NetworkConnectionHelper
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetLastLocalSchoolScoreUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.GetSchoolScoresUseCase
import com.example.a20230301_jorgeespinoza_nycschools.domain.SaveLocalSchoolScoreUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolScoreViewModel @Inject constructor(
    private val getSchoolScoresUseCase: GetSchoolScoresUseCase,
    private val getLastLocalSchoolScoreUseCase: GetLastLocalSchoolScoreUseCase,
    private val saveLocalSchoolScoreUseCase: SaveLocalSchoolScoreUseCase,
    private val network: NetworkConnectionHelper
) : ViewModel() {

    val modelSchoolScore = MutableLiveData<List<SchoolScore>>()
    val isLoading = MutableLiveData<Boolean>()
    val networkConnection = MutableLiveData(true)
    fun getLastLocalSchoolScore( dbn: String ){
        viewModelScope.launch(Dispatchers.IO) {
            if(dbn.isNotEmpty()) {
                val scores = mutableListOf<SchoolScore>()
                getLastLocalSchoolScoreUseCase( dbn ).collect{ score ->
                    scores.add(SchoolScore(score.reading_score, score.math_score, score.writing_score))
                    if( dbn == score.dbn )
                        modelSchoolScore.postValue(scores)
                    else
                        if( network.checkInternetConnection() )
                            getSchoolScore(dbn)
                        else
                            networkConnection.postValue(false)
                }
            }
        }
    }

    fun getSchoolScore( dbn: String ){
        viewModelScope.launch{
            if(dbn.isNotEmpty()) {
                isLoading.postValue(true)
                var scores = getSchoolScoresUseCase( dbn )
                if (scores.isEmpty())
                    scores = listOf(SchoolScore("-","-","-"))
                else
                    saveLocalSchoolScoreUseCase( SchoolScoreLocal(dbn, scores[0].reading, scores[0].math, scores[0].writing) )
                modelSchoolScore.postValue(scores)
                isLoading.postValue(false)
            }
        }
    }
}