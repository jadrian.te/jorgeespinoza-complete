package com.example.a20230301_jorgeespinoza_nycschools.ui.view

import android.os.Build
import android.os.Build.VERSION
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.example.a20230301_jorgeespinoza_nycschools.R
import com.example.a20230301_jorgeespinoza_nycschools.databinding.ActivitySchoolDetailsBinding
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import com.example.a20230301_jorgeespinoza_nycschools.ui.util.toast
import com.example.a20230301_jorgeespinoza_nycschools.ui.viewmodel.SchoolScoreViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity: AppCompatActivity() {

    private lateinit var  binding: ActivitySchoolDetailsBinding

    private val schoolScoreViewModel: SchoolScoreViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_details)
        binding = ActivitySchoolDetailsBinding.inflate( layoutInflater )
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        if(bundle != null)
        {
            val schoolDomain: SchoolDomain? = if (VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                bundle.getParcelable("school", SchoolDomain::class.java)
            else
                @Suppress("DEPRECATION")
                bundle.getParcelable("school")

            if (schoolDomain != null) {
                binding.tvName.text = schoolDomain.name
                binding.tvOverview.text = schoolDomain.overview
                binding.tvLocation.text = schoolDomain.location
                binding.tvPhone.text = schoolDomain.phone
                binding.tvEmail.text = schoolDomain.email
                binding.tvWebsite.text = schoolDomain.website
                schoolScoreViewModel.getLastLocalSchoolScore( schoolDomain.dbn ?: "" )
            }
        }

        schoolScoreViewModel.modelSchoolScore.observe( this, Observer {
            binding.tvReading.text = it[0].reading+"\nRead"
            binding.tvWriting.text = it[0].writing+"\nWrite"
            binding.tvMath.text = it[0].math+"\nMath"
        } )

        schoolScoreViewModel.isLoading.observe( this, Observer {
            binding.prgBar.isVisible = it
        } )

        schoolScoreViewModel.networkConnection.observe(this, Observer {
            if(!it)
                toast( getString( R.string.internet_connection ) )
        })
    }
}