package com.example.a20230301_jorgeespinoza_nycschools.data.repository

import com.example.a20230301_jorgeespinoza_nycschools.data.api.SchoolScoreService
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import com.example.a20230301_jorgeespinoza_nycschools.data.provider.SchoolScoreProvider
import javax.inject.Inject

class SchoolScoreRepository @Inject constructor( private val api: SchoolScoreService, private val schoolScoreProvider: SchoolScoreProvider) {

    suspend fun getAllScores(dbn: String): List<SchoolScore>{
        val response: List<SchoolScore> = api.getSchoolScore( dbn )
        schoolScoreProvider.scores = response
        return response
    }
}