package com.example.a20230301_jorgeespinoza_nycschools.data.api

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SchoolService @Inject constructor(private val api: ClientAPI) {

    suspend fun getAllSchools(): List<SchoolModel> {
        return withContext(Dispatchers.IO) {
            val response = api.getSchools()
            response.body() ?: emptyList()
        }
    }
}