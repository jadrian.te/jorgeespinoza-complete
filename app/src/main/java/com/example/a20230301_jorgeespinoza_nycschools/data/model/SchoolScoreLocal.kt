package com.example.a20230301_jorgeespinoza_nycschools.data.model

data class SchoolScoreLocal(val dbn: String, val reading_score: String, val math_score: String, val writing_score: String )