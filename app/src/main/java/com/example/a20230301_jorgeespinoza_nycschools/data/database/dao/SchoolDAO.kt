package com.example.a20230301_jorgeespinoza_nycschools.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.SchoolEntity

@Dao
interface SchoolDAO {
    @Query("SELECT * FROM schools ORDER BY id ASC")
    suspend fun getAllSchools(): List<SchoolEntity>

    @Insert( onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllSchools( schools: List<SchoolEntity> )

    @Query("DELETE FROM schools")
    suspend fun deleteAllSchools()
}