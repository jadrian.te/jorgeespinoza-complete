package com.example.a20230301_jorgeespinoza_nycschools.data.repository

import android.content.Context
import androidx.datastore.preferences.*
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScoreLocal
import kotlinx.coroutines.flow.map

private const val DSP = "SCHOOL_SCORE"
private val DBN = stringPreferencesKey("DBN")
private val READING_SCORE = stringPreferencesKey("READING_SCORE")
private val MAT_SCORE = stringPreferencesKey("MAT_SCORE")
private val WRITING_SCORE = stringPreferencesKey("WRITING_SCORE")

private val Context.dataStore by preferencesDataStore( DSP )

class DataPreferencesRepository(private val context: Context) {

    suspend fun saveScore( schoolScoreLocal: SchoolScoreLocal ){
        context.dataStore.edit { preferences ->
            preferences[DBN] = schoolScoreLocal.dbn
            preferences[READING_SCORE] = schoolScoreLocal.reading_score
            preferences[MAT_SCORE] = schoolScoreLocal.math_score
            preferences[WRITING_SCORE] = schoolScoreLocal.writing_score
        }
    }

    fun getScore() = context.dataStore.data.map { preferences ->
        SchoolScoreLocal(
            dbn = preferences[DBN].orEmpty(),
            reading_score =  preferences[READING_SCORE].orEmpty(),
            math_score = preferences[MAT_SCORE].orEmpty(),
            writing_score = preferences[WRITING_SCORE].orEmpty()
        )
    }
}