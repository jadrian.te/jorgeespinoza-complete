package com.example.a20230301_jorgeespinoza_nycschools.data.model

import android.os.Parcelable
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.SchoolEntity
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolModel (val dbn: String?, @SerializedName("school_name") val name: String?, val location: String?, @SerializedName("phone_number") val phone: String?, @SerializedName("school_email") val email: String?, val website: String?, @SerializedName("overview_paragraph") val overview: String? ): Parcelable

fun SchoolModel.toModel() = SchoolEntity( dbn = dbn, name = name, location = location, phone = phone, email = email, website = website, overview = overview )