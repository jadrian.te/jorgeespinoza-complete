package com.example.a20230301_jorgeespinoza_nycschools.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain

@Entity("schools")
data class SchoolEntity(@PrimaryKey(autoGenerate = true) val id: Int = 0, @ColumnInfo("dbn") val dbn: String?, @ColumnInfo("name") val name: String?, @ColumnInfo("location") val location: String?, @ColumnInfo("phone") val phone: String?, @ColumnInfo("email") val email: String?, @ColumnInfo("website") val website: String?, @ColumnInfo("overview") val overview: String?)

fun SchoolDomain.toEntity() = SchoolEntity( dbn = dbn, name = name, location = location, phone = phone, email = email, website = website, overview = overview )

fun SchoolEntity.toModel() = SchoolModel( dbn = dbn, name = name, location = location, phone = phone, email = email, website = website, overview = overview )