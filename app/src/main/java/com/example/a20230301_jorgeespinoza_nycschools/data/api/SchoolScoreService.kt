package com.example.a20230301_jorgeespinoza_nycschools.data.api

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SchoolScoreService @Inject constructor(private val api: ClientAPI) {

    suspend fun getSchoolScore( dbn: String ): List<SchoolScore> {
        return withContext(Dispatchers.IO) {
            val response = api.getSchoolScore( dbn )
            response.body() ?: emptyList()
        }
    }
}