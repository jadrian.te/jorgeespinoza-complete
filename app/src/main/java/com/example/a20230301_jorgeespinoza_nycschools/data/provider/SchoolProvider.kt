package com.example.a20230301_jorgeespinoza_nycschools.data.provider

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolProvider @Inject constructor() {
        var schoolModels: List<SchoolModel> = emptyList()
        val schoolsStoredLocals: List<SchoolModel> = listOf(
/*                SchoolModel(
                        "02M260",
                        "Clinton School Writers & Artists, M.S. 260",
                        "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
                        "212-524-4360",
                        "admissions@theclintonschool.net",
                        "www.theclintonschool.net",
                        "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
                )*/
        )
}