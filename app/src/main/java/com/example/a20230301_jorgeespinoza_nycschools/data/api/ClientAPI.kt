package com.example.a20230301_jorgeespinoza_nycschools.data.api

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ClientAPI {
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<SchoolModel>>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolScore(@Query("dbn") dbn: String): Response<List<SchoolScore>>
}