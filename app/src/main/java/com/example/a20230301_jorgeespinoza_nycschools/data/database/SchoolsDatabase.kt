package com.example.a20230301_jorgeespinoza_nycschools.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.a20230301_jorgeespinoza_nycschools.data.database.dao.SchoolDAO
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.SchoolEntity

@Database( entities = [SchoolEntity::class], version = 2 )
abstract class SchoolsDatabase: RoomDatabase() {
    abstract fun getSchoolDAO(): SchoolDAO
}