package com.example.a20230301_jorgeespinoza_nycschools.data.repository

import com.example.a20230301_jorgeespinoza_nycschools.data.api.SchoolService
import com.example.a20230301_jorgeespinoza_nycschools.data.database.dao.SchoolDAO
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.SchoolEntity
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.toModel
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import com.example.a20230301_jorgeespinoza_nycschools.data.provider.SchoolProvider
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.toDomain
import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private val api: SchoolService,
    private val schoolProvider: SchoolProvider,
    private val schoolDAO: SchoolDAO ) {

    suspend fun getAllSchoolsFromAPI(): List<SchoolDomain>{
        val response: List<SchoolModel> = api.getAllSchools()
        schoolProvider.schoolModels = response
        return response.map { it.toDomain() }
    }

    suspend fun getAllSchoolsFromDatabase(): List<SchoolDomain>{
        val response: List<SchoolEntity> = schoolDAO.getAllSchools()
        schoolProvider.schoolModels = response.map { it.toModel() }
        return response.map { it.toDomain() }
    }

    suspend fun saveAllSchools( schools: List<SchoolEntity> ){
        schoolDAO.insertAllSchools( schools )
    }

    suspend fun deleteAllSchools(){
        schoolDAO.deleteAllSchools()
    }

}