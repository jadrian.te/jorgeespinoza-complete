package com.example.a20230301_jorgeespinoza_nycschools.data.model

import com.google.gson.annotations.SerializedName

data class SchoolScore ( @SerializedName("sat_critical_reading_avg_score") val reading: String, @SerializedName("sat_math_avg_score") val math: String, @SerializedName("sat_writing_avg_score") val writing: String )