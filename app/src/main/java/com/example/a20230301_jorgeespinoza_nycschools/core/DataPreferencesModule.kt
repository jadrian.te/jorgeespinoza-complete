package com.example.a20230301_jorgeespinoza_nycschools.core

import android.content.Context
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.DataPreferencesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataPreferencesModule {
    @Singleton
    @Provides
    fun getDataPreferences( @ApplicationContext context: Context ) = DataPreferencesRepository( context )
}