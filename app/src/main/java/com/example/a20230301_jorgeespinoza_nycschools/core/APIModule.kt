package com.example.a20230301_jorgeespinoza_nycschools.core

import com.example.a20230301_jorgeespinoza_nycschools.data.api.ClientAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object APIModule {

    @Singleton
    @Provides
    fun getRetrofit(): Retrofit {
        val baseUrl = "https://data.cityofnewyork.us/resource/"
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client( okHttpClient.build() )
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun getClientAPI(retrofit: Retrofit): ClientAPI{
        return retrofit.create(ClientAPI::class.java)
    }
}