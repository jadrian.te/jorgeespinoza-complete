package com.example.a20230301_jorgeespinoza_nycschools.core

import android.content.Context
import com.example.a20230301_jorgeespinoza_nycschools.data.util.NetworkConnectionHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkConnectionModule {
    @Singleton
    @Provides
    fun getNetworkConnectionInformation( @ApplicationContext context: Context) = NetworkConnectionHelper(context)
}