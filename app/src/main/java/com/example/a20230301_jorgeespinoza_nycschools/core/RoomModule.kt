package com.example.a20230301_jorgeespinoza_nycschools.core

import android.content.Context
import androidx.room.Room
import com.example.a20230301_jorgeespinoza_nycschools.data.database.SchoolsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {
    private const val SCHOOLS_DATABASE_NAME = "schools_database"
    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, SchoolsDatabase::class.java, SCHOOLS_DATABASE_NAME).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideSchoolDAO(db: SchoolsDatabase) = db.getSchoolDAO()

}