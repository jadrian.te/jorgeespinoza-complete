package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScoreLocal
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.DataPreferencesRepository
import javax.inject.Inject

class SaveLocalSchoolScoreUseCase @Inject constructor(private val repository: DataPreferencesRepository){
    suspend operator fun invoke(schoolScoreLocal: SchoolScoreLocal) = repository.saveScore( schoolScoreLocal )
}