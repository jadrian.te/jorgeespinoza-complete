package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScoreLocal
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.DataPreferencesRepository
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.SchoolScoreRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLastLocalSchoolScoreUseCase @Inject constructor(private val repository: DataPreferencesRepository){
    suspend operator fun invoke(dbn: String): Flow<SchoolScoreLocal> = repository.getScore()
}