package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.provider.SchoolProvider
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.toDomain
import javax.inject.Inject

class SearchSchoolsUseCase @Inject constructor(private val schoolProvider: SchoolProvider){

    operator fun invoke( school: String ): List<SchoolDomain>?  {
        var schoolsAux: List<SchoolDomain>? = emptyList()
        if(schoolProvider.schoolModels.isNotEmpty()) {
            schoolsAux = schoolProvider.schoolModels.map { it.toDomain() }.filter {
                it.name?.lowercase()?.contains( school.lowercase() ) ?: false
                        || it.location?.lowercase()?.contains( school.lowercase() ) ?: false
            }
        }
        return if( schoolsAux.isNullOrEmpty() )
            null
        else
            schoolsAux
    }
}