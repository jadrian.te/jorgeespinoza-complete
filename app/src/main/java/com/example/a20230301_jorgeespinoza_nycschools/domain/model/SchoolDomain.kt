package com.example.a20230301_jorgeespinoza_nycschools.domain.model

import android.os.Parcelable
import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.SchoolEntity
import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SchoolDomain(val dbn: String?, val name: String?, val location: String?, val phone: String?, val email: String?, val website: String?, val overview: String?) :Parcelable

fun SchoolModel.toDomain() = SchoolDomain( dbn, name, location, phone, email, website, overview )

fun SchoolEntity.toDomain() = SchoolDomain( dbn, name, location, phone, email, website, overview )