package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.model.SchoolScore
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.SchoolScoreRepository
import javax.inject.Inject

class GetSchoolScoresUseCase @Inject constructor( private val repository: SchoolScoreRepository) {

    suspend operator fun invoke(dbn: String): List<SchoolScore> = repository.getAllScores(dbn)
}