package com.example.a20230301_jorgeespinoza_nycschools.domain

import com.example.a20230301_jorgeespinoza_nycschools.data.database.entity.toEntity
import com.example.a20230301_jorgeespinoza_nycschools.data.provider.SchoolProvider
import com.example.a20230301_jorgeespinoza_nycschools.data.repository.SchoolRepository
import com.example.a20230301_jorgeespinoza_nycschools.data.util.NetworkConnectionHelper
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.SchoolDomain
import com.example.a20230301_jorgeespinoza_nycschools.domain.model.toDomain
import javax.inject.Inject

class GetSchoolsUseCase @Inject constructor(
    private val repository: SchoolRepository,
    private val schoolProvider: SchoolProvider,
    private val network: NetworkConnectionHelper) {

    suspend operator fun invoke(): List<SchoolDomain> {
        var schools = emptyList<SchoolDomain>()
        if( network.checkInternetConnection() )
            schools = repository.getAllSchoolsFromAPI()
        if (schools.isEmpty()) {
            schools = repository.getAllSchoolsFromDatabase()
            if (schools.isEmpty())
                schools = schoolProvider.schoolsStoredLocals.map { it.toDomain() }
        }
        else {
            repository.deleteAllSchools()
            repository.saveAllSchools( schools.map { it.toEntity() } )
        }
        return schools
    }
}