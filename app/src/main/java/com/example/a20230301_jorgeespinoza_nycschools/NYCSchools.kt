package com.example.a20230301_jorgeespinoza_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchools:Application()